## Chat Application using NodeJs and Socket.io
This project consists of main index file called `server.js` which holds the code for initializing the server along with socket.io
To run this application 
clone the repo
```
git clone https://Charan123456@bitbucket.org/Charan123456/w06.git
```

Navigate to the directory
```
cd w06
```

Install the dependencies using the following line
```bash
npm install
```

Start the server
```
node server.js
```

Once you see a message on the console, open the url `http://127.0.0.1.8081` in two windows to test the chat application. Voila!!!

